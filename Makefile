.PHONY: all
all:
	date > build.log
	zola build 2>&1 | tee -a build.log
	mv build.log public/

deploy: all
	rsync -crtvz --delete ./public/ anaproy.nl:/home/www/anaproy.nl/html/gildebuurt/
