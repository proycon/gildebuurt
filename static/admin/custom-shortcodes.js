CMS.registerEditorComponent({
  id: "img",
  label: "Afbeelding (geavanceerd)",
  fields: [
    {
      name: "path",
      label: "Bestand",
      widget: "file",
    },
    {
      name: "class",
      label: "Positionering",
      widget: "select",
      options: ["normaal", "links","rechts","passend","breed","links klein","rechts klein" ]
    },
    {
      name: "alt",
      label: "Beschrijving",
      widget: "string",
    },
  ],
  pattern: /{{ img\( ?path="([-_.:a-zA-Z0-9/ ]+)"(, alt="([a-zA-Z][-_.:a-zA-Z0-9/ ]*)")?(, class="([a-zA-Z][-_.:a-zA-Z0-9 ]*)")? ?\) }}/,
  fromBlock: function(match) {
    return {
      path: match[1],
      alt: match[3],
      class: match[5],
    };
  },
  toBlock: function(obj) {
    var path = obj.path.replace("/static/img/","");
    var path = obj.path.replace("/img/","");
    return `{{ img(path="${path}", alt="${obj.alt}", class="${obj.class}") }}`;
  },
  toPreview: function(obj) {
    var path = obj.path.replace("/static/img/","");
    var path = obj.path.replace("/img/","");
    return `<img src="${base_url}/img/${path}" class="${obj.class}" />`;
  },
});
