+++
title = "Contact"
sort_by = "weight"
weight = 9
template = "contact.html"
+++

Neem contact met ons op als u:

* een plekje wilt in onze moestuin (u komt dan op de wachtlijst).
* een activiteit wilt organiseren als vrijwilliger.
* een bijdrage heeft voor de website (bv. een artikel over een onderwerp in de wijk of een leuke foto).
* een vraag, idee, of opmerking heeft

U kunt ons rechtstreeks mailen op <info@gildebuurteindhoven.nl> of door het onderstaande
contactformulier in te vullen (vergeet in dat geval ook de controlevraag niet!).



