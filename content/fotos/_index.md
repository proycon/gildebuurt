+++
title = "Foto's"
description = "Leuke foto's uit de buurt"
template = "section.html"

[extra]
gallery = [
    { caption = "Unity Festival of Lights - Indiase Dansact tijdens het festival",  date= 2024-12-07T20:03:00+01:00, path = "img/2024-12-07-unityfestival3.jpg" },
    { caption = "Unity Festival of Lights - Alle organisatoren en hosts op het podium",  date= 2024-12-07T20:02:00+01:00, path = "img/2024-12-07-unityfestival2.jpg" },
    { caption = "Unity Festival of Lights - We organiseerden samen met Stiching Sanskriti & Sanskar een groot feest bij het CKE dat de vreugde en tradities van Diwali, Sinterklaas en Kerstmis in één magische avond samenbrengt. Burgemeester Jeroen Dijsselbloem was aanwezig tijdens de openingsceremonie.",  date= 2024-12-07T20:01:00+01:00, path = "img/2024-12-07-unityfestival.jpg" },
    { caption = "Griezelen in de buurttuin tijdens Halloween!",  date= 2024-10-31T20:01:00+02:00, path = "img/2024-10-31-halloween2.jpg" },
    { caption = "We hadden een leuke halloween met een looproute vol enge attracties door de buurt!",  date= 2024-10-31T20:01:00+02:00, path = "img/2024-10-31-halloween.jpg" },
    { caption = "Feestelijke opening van moskee Arrahmaan in onze buurt",  date= 2024-09-29T12:01:00+02:00, path = "img/2024-09-29-opening-moskee-2.jpg" },
    { caption = "Burgemeester Dijsselbloem opent de nieuwe moskee 'Arrahmaan' aan de visserstraat.",  date= 2024-09-29T12:00:00+02:00, path = "img/2024-09-29-opening-moskee.jpg" },
    { caption = "De lichtjesroute is weer begonnen en onze verlichting bij de buurttuin staat ook weer aan", date = 2024-09-21T21:05:00+02:00, path = "img/2024-09-21-lichtjesroute.jpg" },
    { caption = "Onze stichting is aanwezig bij 'Connecting Cultures', een open discussie met diverse culturen, bewoners/buurtstichtingen, waar wij met elkaar kijken waar de uitdagingen liggen en vooral ook wat goed gaat. Mooi om te zien dat er in Eindhoven zoveel wil is om samen ons Eindhoven mooier, fijner en diverser te maken door met elkaar te leven in plaats van naast elkaar.", date = 2024-08-24T20:00:00+02:00, path = "img/2024-08-24-connecting-cultures.jpg" },
    { caption = "Tuinierders hard aan het werk op onze maandelijke opruin/snoei/opschoon ochtend", date = 2024-08-18T11:00:00+02:00, path = "img/2024-08-18-maandelijkse-tuindag.jpg" },
    { caption = "De man met de zeis heeft de heg van de moestuin weer eens gefatsoeneerd op de maandelijke tuindag", date = 2024-08-20T10:14:00+02:00, path = "img/2024-08-18-man-met-zeis.jpg" },
    { caption = "We hadden als Stichting twee leuke kraampjes op het Kruisstraat Originals Festival", date = 2024-05-22T21:24:27+02:00, path = "img/2024-08-16-kruisstraat-originals.jpg" },
    { caption = "Als één van de laatste huisjes ondergaat de voormalige buurtsalon aan de Bakkerstraat 4 de sloop, alleen een stukje gevel staat hier nog.", date = 2024-05-22T21:24:27+02:00, path = "img/2024-05-22 Sloopwerkzaamheden bakkerstraat.jpg" },
    { caption = "Het vernieuwde bestuur van de stichting op de foto in de buurttuin", date = 2024-05-19T16:57:27+02:00, path = "img/2024-05-19-bestuursfoto.jpg" },
    { caption = "Het nieuwe bestuur van het Bewoners Initiatief Gildebuurt poseert voor de foto", date = 2023-04-27T11:45:27+02:00, path = "img/2023-04-27 het nieuwe bestuur.jpg" },
    { caption = "Tulpen in bloei in buurttuin 'bij de pastoor'", date = 2022-04-24T12:45:27+02:00, path = "img/2022-04-24 tulpen in moestuin bij de pastoor.jpg" },
    { caption = "Lentebloesem in de spinnerstraat", date = 2022-04-14T09:00:27+02:00, path = "img/2022-04-14 lentebloesem in de spinnerstraat.jpg" },
    { caption = "Minister Hugo de Jonge op bezoek in de Gildebuurt, foto vanuit Bakkerstraat 4", date = 2022-03-15T12:00:27+01:00, path = "img/minister-op-bezoek.jpg" },
    { caption = "De Montgomery-tunnel kleurt geel-blauw naar aanleiding van verschrikkelijke gebeurtenissen in Oekraïne", date = 2022-02-25T21:58:27+01:00, path = "img/2022-02-25 steun voor oekraine.jpg" },
    { caption = "Winterweer en bouwactiviteiten bij de Fontys", date = 2022-02-25T16:32:00+01:00, path = "img/2022-02-25 winterweer en bouw fontys.jpg" },
    { caption = "uitzicht vanuit een dakraam in de spinnerstraat", date = 2021-09-27T11:48:55+01:00, path = "img/2021-09-27 uitzicht vanuit een dakraam in de spinnerstraat.jpg" },
    { caption = "Nieuwe plantenbakken in de spinnerstraat en visserstraat", date = 2021-11-14T11:48:55+01:00, path = "img/2021-11-14 Nieuwe plantenbakken in de spinnerstraat en visserstraat.jpg" },
    { caption = "Buurtgenoot met kip in de buurttuin", path = "img/activiteiten-pastoor-esther.jpg" },
    { caption = "Feestelijke opening van de buurttuin 'Bij de Pastoor'", date = 2020-09-17T18:10:55+01:00, path = "img/opening-buurttuin.jpg" },
    { caption = "Speech tijdens de opening van de buurttuin 'Bij de Pastoor'", date = 2020-09-17T18:10:55+01:00, path = "img/speech-buurttuin.jpg" },
    { caption = "Grote schoonmaakactie in de buurt", date = 2019-10-27T12:10:55+01:00, path = "img/incidentele-activiteit-schoonmaakactie.jpg" },
    { caption = "Info doedag 2019: Ondanks regen en wind hing er een gezellige sfeer met timmerende & goochelende kinderen en haar-vlechtende en knutselende moeders! De koks van het Leger des Heils hadden een lekker maaltje bereid!", date = 2019-08-18T12:00:00+01:00, path = "img/infodoedag2019.jpg" },
    { caption = "Bezoek aan de dierentuin met buurtgenoten", path = "img/incidentele-activiteit-dierentuin.jpg" },
    { caption = "De spinnerstraat bij sneeuw", date = 2017-12-10T13:02:55+01:00, path = "img/spinnerstraat-sneeuw.jpg" },
    { caption = "Barbecue in de oude moestuin (tot 2019)", date = 2016-05-10T18:55:00+02:00, path = "img/bbq-oude-moestuin.jpg" }
]
+++

Hier een aantal leuke foto's uit de buurt. Heb je ook een leuke foto om erbij te zetten? [Stuur het ons](/contact/)!

