+++
title = "Gildebuurt Eindhoven"
sort_by = "weight"

[extra]
motto = "Ons doel is om samen met buurtbewoners leuke en gezellige activiteiten te ontwikkelen en om zaken op te pikken waardoor de wijk opfleurt op allerlei fronten."
+++

{{ img(path="gildebuurtkaart.jpg", alt="Kaart van de Gildebuurt", class="rechts klein" ) }}

De **Gildebuurt** is een levendige buurt in de wijk Oud-Woensel in Eindhoven, vlak boven het centrum, ingesloten tussen de gezellig drukke Kruisstraat en de Veldmaarschalk Montgomerylaan.

Wij zijn het **Bewoners Initiatief Gildebuurt**, een buurtstichting vóór en dóór de bewoners zelf. We zetten ons sinds 2012 in voor onze buurt. Zo organiseren we activiteiten en hebben we als grootste trots onze **[buurttuin “bij de Pastoor”](activiteiten/buurttuin)** aan de Pastoriestraat.

Op deze website proberen we je op de hoogte te houden van wat er in onze buurt speelt. Hier kan je altijd de laatste informatie vinden over de verschillende activiteiten die georganiseerd worden. Hierbij zijn altijd alle buurtbewoners welkom. Samen proberen we de buurt gezelliger te maken. Heb je vragen, tips, ideeën, of wil je je inschrijven voor een activiteit? Neem dan [contact met ons op](/contact/)!

