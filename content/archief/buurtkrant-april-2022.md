+++
title = "Buurtkrant april 2022"
description = "De huis-aan-huis buurtkrant voor de wijk"
template = "page.html"
date = 2022-03-15T12:35:05.373Z

[extra]

[[extra.attachments]]
name = "Buurtkrant april 2022"
path = "/static/attachments/buurtkrant-2022-04.pdf"
+++

We proberen elk kwartaal een buurtkrant te laten verschijnen; vol informatie en activiteiten die hier in en rond de Gildebuurt spelen.

Deze wordt verspreid in de buurt, maar u kunt hem hier ook on-line bekijken. Veel leesplezier!
