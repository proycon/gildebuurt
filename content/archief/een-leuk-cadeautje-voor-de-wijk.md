+++
title = "Een leuk cadeautje voor de wijk"
description = "We kregen een bankje van de bouwvakkers van de HOV"
date = 2019-05-05T10:48:55.000Z

[extra]
motto = ""
+++

{{ img(path="bankje-hov.jpg" class="passend") }}

Uit handen van Mark Pijnenburg werd er een bankje geschonken aan de buurtsalon gemaakt door de bouwvakkers van de HOV.

“Met liefde gemaakt” was het commentaar van Mark. Wethouder Yasin Torunoğlu en Loes Wessel, die getuigen waren van deze actie, vonden het ook geweldig.

Een blijvende herinnering en gemaakt van de bekisting van de tunnel! Leuk om te onthouden!

