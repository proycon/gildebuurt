+++
title = "Buurtkrant april 2021"
description = "De huis-aan-huis buurtkrant voor de wijk"
template = "page.html"
date = 2022-03-04T11:35:05.373Z

[extra]

[[extra.attachments]]
name = "Buurtkrant april 2021"
path = "/static/attachments/buurtkrant-april-gildebuurt-drukversie-definitief-webversie.pdf"
+++
We proberen elk kwartaal een buurtkrant te laten verschijnen; vol informatie en activiteiten die hier in en rond de Gildebuurt spelen.

Deze is al verspreid in de buurt, maar u kunt hem hier ook on-line bekijken. Veel leesplezier!