+++
title = "Vernieuwde website & contactmogelijkheden"
description = "Met het aantreden van het nieuwe bestuur heeft ook de website weer een update gekregen"
template = "page.html"
date = 2023-06-26T20:34:01.695Z
+++

{{ img(path="website_screenshot.jpg", alt="Een screenshot van de website", class="rechts klein") }}

De gildebuurt heeft al tijden haar eigen plekje op het web, namelijk
<https://gildebuurteindhoven.nl>. We schreven hier vorig jaar ook al [een
stukje](/archief/website/) over. Tegelijk met het aantreden van het nieuwe
bestuur is de website ook weer wat vernieuwd. Het design is wat opgepoetst en
de inhoud wat aangepast. Zo wordt je op de beginpagina nu begroet met een
prominente foto uit de buurt, willekeurig gepresenteerd uit een set van zo'n
elf foto's die we onlangs in de buurt gemaakt hebben.

Verder is de website met opzet minimalistisch gehouden. Je vindt er in ieder
geval informatie terug over lopende en aankomende activiteiten, het laatste
nieuws, en verdere foto's. Als je zelf een leuke bijdrage hebt is dat zeer welkom!

{{ img(path="qr.png", alt="QR code voor gildebuurteindhoven.nl", class="rechts klein") }}

De website heeft nu ook een contactformulier, neem of via dit formulier
of gewoon via e-mail (<info@gildebuurteindhoven.nl>) contact met ons op als je:

* een plekje wilt in onze moestuin (je komt dan op de wachtlijst).
* een activiteit wilt organiseren als vrijwilliger.
* een bijdrage hebt voor de website (bv. een artikel over een onderwerp in de wijk of een leuke foto).
* een vraag, idee, of opmerking hebt

