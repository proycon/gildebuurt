+++
title = "Uitvoering buurtbudget"
description = "De eerste ideeën voor het buurtbudget worden uitgevoerd"
template = "page.html"
date = 2022-03-13T12:08:01.695Z
+++

De gemeente rapporteert in haar *"Nieuwsbrief Oud-Woensel"* van 21 December 2021 het volgende:

In oktober heeft de buurt digitaal en bij de stembussen in de wijk gestemd op
hun favoriete ideeën. Het Buurt Budget van €50.000,- is als volgt verdeeld:

* Meer prullenbakken €15.000
* Een zebrapad (voor schoolkinderen) en streetart op de Kruisstraat €10.600
* Een Oud-Woensels zaalvoetbalteam voor jongeren €4.000
* Sfeermakers voor op de Kruisstraat – Woenselse Markt €4.000
* Plantenbakken om het verkeer af te remmen in Hemelrijken €15.000

Samen met de indieners zijn we nu in gesprek over wat er precies nodig is en hoe
we het gaan uitvoeren. Voor sommige ideeën gaat het al heel snel:
de prullenbakken worden dit jaar nog geplaatst en het zaalvoetbalteam heeft al
een mooi logo gemaakt! Andere ideeën zoals die op het thema 'verkeer' hebben
mogelijk meer tijd of aanvullende besluitvorming van de gemeente nodig. We
informeren de buurt via de nieuwsbrief als we meer weten over de uitvoering.

## Wat vind je van het project? Vul de enquête in.

Met het project Buurt Budget proberen we het instrument burgerbegroting uit. Als
het succesvol is, kunnen we het vaker inzetten in de stad. Daarom is het
belangrijk om te weten wat bewoners van Oud Woensel vinden van het proces en of
er suggesties zijn. Vul de enquête in via [deze
link](https://www.eindhoven.nl/stad-en-wonen/stadsdelen/stadsdeel-woensel-zuid/oud-woensel/buurt-budget-oud-woensel).

Het kost je ongeveer 5 á 10 minuten. Door het openen van  link ga je akkoord met
het privacybeleid en met het opslaan en analyseren van uw gegevens.

Meer informatie over het project Buurt Budget via: [https://www.eindhoven.nl/stad-en-wonen/stadsdelen/stadsdeel-woensel-zuid/oud-woensel/buurt-budget-oud-woensel](https://www.eindhoven.nl/buurtbudget)
