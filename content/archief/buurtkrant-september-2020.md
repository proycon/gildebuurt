+++
title = "Buurtkrant september 2020"
description = "De huis-aan-huis buurtkrant voor de wijk"
template = "page.html"
date = 2020-09-01T15:24:01.695Z

[extra]

[[extra.attachments]]
path = "/static/attachments/nieuwsbrief-gildebuurt2-september-2020-webversie.pdf"
name = "Buurtkrant september 2020"
+++
Elk kwartaal verschijnt er een buurtkrant; vol informatie en activiteiten die hier in de Gildebuurt spelen.

Deze is al verspreid in de buurt, maar u kunt hem hier ook on-line bekijken. Veel leesplezier!
