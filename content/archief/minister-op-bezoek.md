+++
title = "Minister op bezoek"
description = "Minister Hugo de Jonge brengt een bezoek aan de gildebuurt"
template = "page.html"
date = 2022-03-14T12:00:05.373Z

+++

Minister Hugo de Jonge, ons allen welbekend van de corona persconferenties,
heeft in zijn nieuwe rol als minister van Volkshuisvestiging en Ruimtelijke
Ordering een bezoek gebracht aan de Gildebuurt.

{{ img(path="minister-op-bezoek.jpg" class="passend") }}

{{ img(path="minister-op-bezoek-2.jpg" class="passend") }}




