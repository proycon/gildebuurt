+++
title = "Even voorstellen: De wijk-GGD'ers"
description = "Wat doen deze wijk-GGD'ers?"
template = "page.html"
date = 2022-03-04T11:41:30.930Z

[extra]

[[extra.attachments]]
path = "/static/attachments/flyerwijkggd.pdf"
name = "wijk-GGD'ers"
+++
Soms lukt het iemand door omstandigheden niet om zijn of haar leven op orde te houden.

Zij roepen veel zorg op of veroorzaken overlast.

Maakt u zich zorgen over iemand, of in uw omgeving?

Neem contact op met ons.