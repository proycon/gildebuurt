+++
title = "Schoonmaakdag 27 okt 2019"
date = 2019-10-27T11:48:55+01:00

+++

{{ img(path="schoonmaakdag-2018-3.jpg" class="passend") }}

U kunt zondag 29 oktober 2019 van 10.00u- tot 14.00u weer al uw overbodige of afgedankte spullen inleveren op het pleintje hoek Bakkerstraat/Verwerstraat.

