+++
title = "Buurt opruimdag - 15 september"
description = "Op 15 september organiseren wen een Opruimdag. Vanaf 13:00 tot 17:00 willen we samen met de vrijwilligers de buurt schoon gaan maken. We verzamelen ons bij de buurttuin. Er staat een grote container klaar en achteraf is er een barbecue en film. Meld je aan om mee te doen!"
template = "page.html"
date = 2024-09-06T15:00:00

[extra]
thumbnail = "/img/thumb_opruimdag202409.jpg"
+++

Op 15 september organiseert Stichting Bewoners Initiatief de Buurt Opruimdag.
Vanaf 13:00 tot 17:00 willen we samen met de vrijwilligers de buurt schoon gaan maken. We verzamelen ons bij 
onze buurttuin "Bij de Pastoor" [aan de
Pastoriestraat](https://www.openstreetmap.org/#map=19/51.45260/5.47499).

Rond een uur of 17:00 zullen we de barbecue aan gaan steken waarna we onder het
genot van het eten een film kunnen gaan kijken onder het afdak van de blokhut.

Aanmelden kan via een mailtje naar
[info@gildebuurteindhoven.nl](mailto:info@gildebuurteindhoven.nl) of ons via ons [contactformulier](/contact/).

Er zal tevens een grote container staan waar allerlei afval in gedaan kan worden.

{{ img(path="portaalcontainer.png", alt="de afvalcontainer", class="klein") }}

*Let wel op:* geen chemisch of elektrisch afval (waar accu's e.d. in zitten).

{{ img(path="opruimdag202409.jpg", alt="Buurt opruimdag", class="passend") }}

