+++
title = "Unity Festival of Lights"
description = "Op 7 december organiseren we een groot feest! Wees welkom!"
date = 2024-11-11T12:03:47.377Z

[extra]
thumbnail = "/img/thumb_unityfestival.jpg"
+++


Wij organiseren samen met [Stichting Sanskriti &
Sanskar](https://sanskritiandsanskar.com/) op 7 December 2024 het **"Unity
Festival of Lights"** festival! Het wordt een feest dat de vreugde en tradities
van Diwali, Sinterklaas en Kerstmis in één magische avond samenbrengt. Een
bonte multiculturele mix met licht als verbindende factor! En dat alles in de
lichtstad Eindhoven! 

Er is muziek, dans, eten & drinken. Ook kinderen zijn van harte welkom en
kunnen deelnemen aan leuke activiteiten. We bieden een perfecte gelegenheid
voor verschillende groepen om met elkaar's cultuur kennis te maken en er een
leuk feestje van te maken!

Het feest vindt plaats in bij [Cultuur & Kunst Eindhoven
(CKE)](https://cke.nl/) aan de Pastoor Petersstraat 180 in Eindhoven, van 16:00
tot 21:00. Deelname is geheel gratis maar [registreer je wel even via deze
link](https://eventix.shop/ncucbqk4), want het aantal plekken is niet
onbeperkt.

Dit is tot stand gekomen als samenwerking ontstaan uit een ontmoeting tussen
onze stichting en de Indiase stichting tijdens het [Connecting
Cultures](https://www.ed.nl/eindhoven/expats-en-lokale-eindhovenaren-zoeken-de-verbinding-tijdens-connecting-cultures~a2d26ad1/)
event van Miriam Frosi. Dit evenement is financieel mogelijk gemaakt middels een waardebon, toegekend door de gebiedscommissie Oud-Woensel.

We hopen veel buurtbewoners te mogen verwelkomen!

{{ img(path="unity_festival_of_lights.jpg", alt="Unity Festival of Lights", class="breed") }}
