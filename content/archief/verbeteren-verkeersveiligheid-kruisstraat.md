+++
title = "Verbeteren verkeersveiligheid en aantrekkelijkheid kruisstraat"
description = "Voorstel vanuit de gemeente en bewoners"
template = "page.html"
date = 2022-03-13T12:08:01.695Z
+++

De gemeente rapporteert in haar "Nieuwsbrief Oud-Woensel" van 21 December 2021 het volgende:

De gemeente heeft (naar aanleiding van gesprekken met ondernemers en bewoners)
een voorstel gemaakt van maatregelen om de verkeersveiligheid op de Kruisstraat
en de Woenselse Markt te verbeteren én de aantrekkelijkheid van het gebied te
vergroten. Hiervoor is ook onderzoek gedaan naar het verkeer in het gebied. De
maatregelen zijn o.a.: het omdraaien van de rijrichting in een deel van de
Kruisstraat, het verminderen van parkeerplekken, een ander parkeertarief en het
maken van laad- en losplekken.

Het voorstel kun je bekijken via [deze
webpagina](https://www.eindhoven.nl/stad-en-wonen/stadsdelen/stadsdeel-woensel-zuid/oud-woensel/verkeer-en-inrichting-kruisstraat-woenselse-markt).
Je kunt dan ook officieel je mening geven (een 'zienswijze indienen'). Meer
informatie over waar de zienswijze aan moet voldoen vind je op de webpagina.
Ook lees je daar meer over de planning, de lange termijn en de onderbouwing van
het voorstel.
