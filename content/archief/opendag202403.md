+++
title = "Open dag Buurttuin - 27 maart"
description = "Op woensdag 27 maart tussen 16:00 en 19:00 organiseren we een open dag in buurtuin 'Bij de Pastoor'"
template = "page.html"
date = 2024-03-16T21:24:01.695Z

[extra]
thumbnail = "/img/thumb_opendag202403.jpg"
+++

Op woensdag 27 maart tussen 16:00 en 19:00 organiseert Stichting
Bewonersinitiatief Gildebuurt Eindhoven samen met [Wij Eindhoven](https://www.wijeindhoven.nl/) een open dag in
de buurttuin "Bij de Pastoor" [aan de
Pastorielaan](https://www.openstreetmap.org/#map=19/51.45260/5.47499).

We hopen hiermee een gelegenheid te bieden waar bewoners van de Gildebuurt
kennis kunnen maken met elkaar, de stichting, en een mooi stukje groen in de
stad, onder het genot van een kopje koffie of thee!

{{ img(path="opendag202403.jpg", alt="Open dag Buurttuin Bij de Pastoor", class="passend") }}
