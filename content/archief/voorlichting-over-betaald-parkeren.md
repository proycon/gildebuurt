+++
title = "Voorlichting over (Betaald) Parkeren 30 maart"
description = "Parkeren wordt een steeds duidelijker issue. Wat kunnen bewoners hieraan doen? "
template = "page.html"
date = 2022-03-04T15:47:58.243Z
+++
In een gedeelte van de Gildebuurt staan steeds meer de borden "Betaald Parkeren"

De Gemeente geeft, voor bewoners in die straten waar nog geen betaald parkeren is ingevoerd, een voorlichting over wat de ontwikkelingen daarin zullen of kunnen betekenen voor de buurt. Ook in verband met de bouwontwikkelingen voor de nabije toekomst van het Gildekwartier (Visserstraat) en de sloop/nieuwbouw in het Noordelijke deel van de buurt.

Woensdag 30 maart van 19.00 – 21.00 uur op Basisschool De Driestam.