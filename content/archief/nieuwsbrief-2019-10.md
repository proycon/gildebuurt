+++
title = "Buurtkrant oktober 2019"
description = "De huis-aan-huis buurtkrant voor de wijk"
date = 2019-10-09T11:48:55+01:00
template = "page.html"

[extra]
attachments = [
    { name = "Buurtkrant oktober 2019", path = "buurtkrant-2019-10.pdf" }
]
+++

Elk kwartaal verschijnt er een buurtkrant; vol informatie en activiteiten die hier in de Gildebuurt spelen.

Deze is al verspreid in de buurt, maar u kunt hem hier ook on-line bekijken. Veel leesplezier!


