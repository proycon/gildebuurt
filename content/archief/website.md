+++
title = "Nieuwe website"
description = "Het Bewoners Initiatief Gildebuurt heeft een vernieuwde website"
template = "page.html"
date = 2022-03-06T22:08:01.695Z
+++

Sinds maart 2022 is de website voor de Gildebuurt grondig vernieuwd! De website
is als vanouds te vinden via
[https://gildebuurteindhoven.nl](https://gildebuurteindhoven.nl). Hier voorzien
we de digitale lezer van allerlei informatie over de buurt, zoals bijvoorbeeld
laatste nieuwtjes, de buurtkranten en aankondigingen of verslagen van
activiteiten die in de buurt ondernomen worden. De website heeft ook een
foto-sectie met foto's van en uit de buurt, en iedereen is welkom wat in te
sturen.

Het is echt een website voor en door de buurt zelf, en staat geheel los van
officiële gemeentelijke webpagina's. Wel kan je via onze buurtsite makkelijk je
weg vinden naar de websites van allerlei instanties en instituten die relevant
zijn voor bewoners van onze buurt.

Een leuk detail is wellicht dat de website nu ook echt vanuit de Gildebuurt
zelf gehost wordt. Het draait dus letterlijk op een server in een woning in onze
buurt en is via glasvezelkabel verbonden met de buitenwereld via het hokje van de
kabelbeheerder tussen de speeltuin en de Spinnerstraat.

Een groot deel van de informatie van de vorige website is al overgezet naar de
nieuwe. Een aantal verouderde en achterhaalde delen zijn wel weggelaten. Bovendien is er
een hoop nieuwe informatie op de website gezet, zoals een stuk over de
buurttuin *"bij de Pastoor"*, die nog geheel ontbrak op de vorige site.

Het bijhouden van de oude website werd steeds lastiger en bezoekers zullen
wellicht gemerkt hebben dat de informatie daar al erg gedateerd was. De nieuwe
site moet het allemaal weer een stuk beheerbaarder maken. Kijk dus vooral
geregeld terug om te zien wat er zo al speelt.

{{ img(path="webdesign.jpg", alt="Aan het werk voor de nieuwe website", class="rechts klein") }}

De maker van de nieuwe site, buurtbewoner Maarten van Gompel, zet in op een
website die zowel voor gebruiker als beheerders simpel van opzet is. Hij vindt
namelijk dat het met het tegenwoordige *World Wide Web* al jaren de verkeerde
kant opgaat. *"Er zijn veel onnodig complexe websites en besloten social media
platforms zoals Facebook en Instagram waar de gebruiker (vaak onbewust)
eigenlijk het product is en ten dienste staat van het platform, wiens doel het
is je zo gericht mogelijk advertenties te presenteren"*, zegt hij, *"en dat
doen ze door je surfgedrag zo veel mogelijk te volgen"*. Op de buurtwebsite zal
je niks van dat alles vinden, de site is door zijn simpelheid in hoge mate
privacy-vriendelijk. Er zijn geen irritante pop-ups, geen advertenties, geen
cookies, geen paywalls, en geen misleidende technieken om je aandacht vast te
houden.

Omdat we een site voor en door de buurt zijn, willen we dat alle buurtbewoners
er hun ei kwijt kunnen. Wil je een leuk stukje schrijven over een onderwerp wat
de buurt aangaat of je eigen interesse heeft? Of heb je een leuke foto uit de
buurt? Stuur het naar [info@gildebuurteindhoven.nl](mailto:info@gildebuurteindhoven.nl) en wij zetten het als het
geschikt is op de website.
