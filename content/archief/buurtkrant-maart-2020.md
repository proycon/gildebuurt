+++
title = "Buurtkrant maart 2020"
description = "De huis-aan-huis buurtkrant voor de wijk"
template = "page.html"
date = 2020-03-01T15:18:45.416Z

[extra]

[[extra.attachments]]
name = "Buurtkrant maart 2020"
path = "/static/attachments/nieuwsbrief-1-2020maart-digitale-versie.pdf"
+++
Elk kwartaal verschijnt er een buurtkrant; vol informatie en activiteiten die hier in de Gildebuurt spelen.

Deze is al verspreid in de buurt, maar u kunt hem hier ook on-line bekijken. Veel leesplezier!