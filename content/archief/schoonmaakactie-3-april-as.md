+++
title = "schoonmaakactie 3 april as"
description = "Schoonmaakdag op zondag 3 april"
template = "page.html"
date = 2022-03-04T12:27:17.194Z
+++
Wil je overtollige spullen kwijt uit je huis, schuur of tuin?

Breng ze dan naar de kraakwagens op 3 april tussen 11.00u-14.00u.

Heb je hiermee hulp nodig? Meld dit om 9.00u aan de helpdesk in de buurtsalon Bakker 4.

**Wil je helpen sjouwen? Meld je aan. Vele handen maken licht werk!**

Spullen over waar je een ander blij mee kunt maken..., leg ze dan op onze "een-dag-marktplaats" tussen de platanen op het pleintje. Ga daar ook snuffelen! 

Op deze dag zijn ook Irene en James, "de toekomstige gezichten" van Thuis aanwezig die vragen beantwoorden.

Ook Janneke Lopata, verbinder van WijEindhoven en collega's  komen kennis maken met de buurt.