+++
title = "Buurt in Beweging"
description = "In het begin van het nieuwe jaar start er in Woensel Zuid weer een nieuwe editie van Buurt in Beweging. "
date = 2024-12-20T10:45:47.377Z

+++

Dat bewegen goed is voor je gezondheid weten we allemaal. Toch kan het lastig zijn om ermee te starten. Buurt in Beweging helpt je daarbij. Tijdens dit programma van twaalf weken maak je kennis met verschillende sporten en beweegaanbieders bij jou in de buurt. Onder begeleiding van een van onze buurtsportcoaches kom je samen met anderen in beweging. Na afloop is er genoeg tijd voor gezelligheid met een drankje.

* **Startdatum:**  7 januari 2025
* **Wanneer:**  Dinsdag van 9.30-11.00u (verzamelen tussen 9.15u-9.30u)
* **Waar:** Wijkcentrum de Werf, van der werffstraat 14 
* **Wie:** Volwassenen
* **Kosten:**  €12 voor het volledige programma van twaalf weken, de rekening wordt via de mail verstuurd 
* **Benodigdheden:**  Zorg ervoor dat je sportkleding aan hebt en houdt er rekening mee dat we buiten gaan sporten. Bij slecht weer zullen we uitwijken naar binnen.

Zie de [Buurt in Beweging website](https://www.eindhovensport.nl/sporten/sporten-voor-doelgroepen/buurt-in-beweging) voor meer informatie en aanmelding. Dit is een gemeentelijk initiatief van Eindhoven Sport.

