+++
title = "Buurttuin \"Bij de Pastoor\""
description = "Sinds 2020 is er een buurttuin aan de Pastoriestraat waar verschillende bewoners een moestuin onderhouden."
date = 2022-03-04T12:03:47.377Z

[extra]
thumbnail = "/img/thumb_moestuin.jpg"

[[extra.gallery]]
path = "img/2024-08-18-maandelijkse-tuindag.jpg"
caption = "Tuinierders aan de slag tijdens de maandelijke tuindag"
date = 2024-08-18T11:03:47.398Z

[[extra.gallery]]
path = "img/activiteiten-pastoor-groenten.jpg"
caption = "De oogst van één van de moestuinen"
date = 2022-03-04T12:03:47.398Z

[[extra.gallery]]
path = "img/activiteiten-pastoor-esther.jpg"
caption = "De buurttuin heeft een heuse kippenren!"
date = 2022-03-04T12:03:47.412Z

[[extra.gallery]]
path = "img/activiteiten-pastoor-lichtsymbool.jpg"
caption = "Herdenkingslicht aan de in de oorlog omgekomen familie Khan, die op deze plek woonden."
date = 2022-03-04T12:03:47.426Z

[[extra.gallery]]
path = "img/activiteiten-pastoor-tomaatzwart.jpg"
caption = "Groente en fruit uit eigen moestuin!"
date = 2022-03-04T12:03:47.439Z

[[extra.attachments]]
path = "/static/attachments/tuinreglement.pdf"
name = "Tuinreglement"
+++
{{ img(path="buurttuin2023.jpg", alt="Een overzichtsfoto van de buurtuin", class="breed") }}

{{ img(path="buurttuin.jpg", alt="Rozen in de buurttuin", class="rechts klein") }}

In 2020 is deze tuin gerealiseerd aan de Pastoriestraat waar vele huizen gesloopt werden voor de aanleg van de
HOV-tunnel.

De tuin heeft aan de ene kant de vorm van een bloem. Aan de andere kant is het een “wandelparkje” met
bankjes. Het eerste deel is bestemd voor moestuinders en schoolkinderen. Dit deel van de tuin is ingericht in vakken.
Het tweede deel is bestemd voor passanten die even in het groen willen zitten met eventueel een kopje koffie of thee
aangereikt door een gastvrouw/-heer.

Het hart van de bloem is een herdenkingsplek voor de in de oorlog omgekomen familie Khan. Op de trottoir liggen de
struikelstenen. De enige overlevende Sonja had een wens om een appelboom te plaatsen op de plek waar haar huis stond. De
vrijwilligers van de Lichtjesroute hebben daar een herdenkingslicht gemaakt. De bijenkorf symboliseert haar vlucht als
peutertje. Een imker uit de wijk smokkelde haar onder een bijenkorf de wijk uit. In de bloem herkent u de Davidster.

3 vakken, in de vorm van bladen,  zijn ingericht als pluktuin; 1 vak als bessentuin; 1 vak als tropentuin. De andere
vakken zijn het domein van de moestuinders. Zij huren ieder “een blad” van het patroon.

De Driestam heeft een cirkel waarin kinderen van de school naar harte lust kunnen
zaaien, kweken en oogsten. Daarnaast staat een kas die gedeeld wordt met de moestuinders. Ook 6 wyandotte krielkippen
zorgen mede voor een sfeer van rust en natuur midden aan de rand van de stad

In 2020 is de tuin feestelijk geopend, zoals u op onderstaande video kunt zien:

{{ video(url="https://download.anaproy.nl/bijdepastoor_opening.mp4") }}

We organiseren elke derde zondag van de maand tussen 11:00 en 14:00 een opruim/snoei/opschoon ochtend voor onze tuinierders, in 2024:

* 21 april 2024
* 19 mei 2024
* 16 juni 2024
* 21 juli 2024
* 18 augustus 2024
* 15 september 2024
* 20 oktober 2024
* 17 november 2024
* 15 december 2024

Hieronder kan je het reglement vinden, en meerdere leuke foto's uit de buurttuin:
