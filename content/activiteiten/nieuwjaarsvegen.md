+++
title = "Nieuwjaarsvegen"
description = "Nieuwjaarsvegen? Dat is op 1 januari samen met je buren (of alleen) het vuurwerkafval in je eigen straat opruimen."
date = 2024-12-20T10:46:47.377Z

[extra]
thumbnail = "/img/thumb_troeptroopers.png"
+++

Nieuwjaarsvegen? Dat is op 1 januari samen met je buren (of alleen) het vuurwerkafval in je eigen straat opruimen.
[Troep Troopers Eindhoven](https://troeptroopers.nl) zet zich in voor een zwerfafvalvrij Eindhoven. Zie [hun website](https://troeptroopers.nl) voor meer informatie over deze actie en doe mee!

