+++
title = "Bouwontwikkelingen"
description = "Er wordt in en om de gildebuurt volop gebouwd de komende jaren. Hier een overzicht van de plannen en belangrijkste ontwikkelingen."
date = 2022-03-13T20:24:55.293Z

[extra]
thumbnail = "/img/thumb_bouwplannen.jpg"

[[extra.attachments]]
path = "/static/attachments/buurtadvies-gildekwartier.pdf"
name = "Buurtadvies voor het Gildekwartier"
+++

Er wordt in de Gildebuurt volop gebouwd de komende jaren. Op deze pagina zie je een overzicht van de belangrijkste
ontwikkelingen.

## Sloop en nieuwbouw huurwoningen Gildebuurt

De huurwoningen rond de wassenaarstraat, slagerstraat, en bakkerstraat zullen verdwijnen en plaatsmaken voor nieuwbouw.
Dit besluit is genomen nadat de bewoners zelf hun stem uitbrachten voor de toekomst van de buurt. Men kon kiezen voor óf renovatie óf nieuwbouw. Beide
kampen hadden evenveel stemmen. In dat geval zou de knoop zoals afgesproken door woningbouwvereniging Wooninc
doorgehakt, en is er voor nieuwbouw gekomen. Lees [op deze
website](https://www.mijn-thuis.nl/nieuws/bewoners-van-de-gildebuurt-stemmen-voor-toekomst-van-de-buurt/) over het
proces en de plannen.

## Gildekwartier

Meubelwinkel Verouden op de verwerstraat verdwijnt en op de plaats komen nieuwe appartementen met een mooie groene
daktuin die publiek toegankelijk zal zijn.

Het Eindhovens Dagblad schreef er vorig jaar [het volgende artikel](https://www.ed.nl/eindhoven/meubelzaak-in-eindhoven-maakt-plaats-voor-182-woningen-dakpark-werkplekken-en-sportvoorziening~a0aea289/) over. De plannen zelf zijn [op de site van de projectontwikkelaar](https://kragtgroep.nl/gildekwartier) uitvoerig
gepresenteerd. Lees ook het [buurtadvies](/attachments/buurtadvies-gildekwartier.pdf) wat na consultatie met de
buurt is opgesteld.

Tekening van de plannen voor het Gildekwartier (©Verouden/Kragt/FAAM/Tom van Tuijn):

{{ img(path="gildekwartier-design.jpg", alt="Tekening van de plannen voor het Gildekwartier, ©Verouden/Kragt/FAAM/Tom van Tuijn", class="passend") }}

Er verscheen wederom [een artikel in het Eindhovens dagblad](https://www.ed.nl/eindhoven/bouw-190-appartementen-in-woensel-start-in-voorjaar-ook-gedeelde-woningen-in-plan-gildekwartier~a6b3488a/) over de aanstaande bouw.
Met daarin de volgende luchtfoto als impressie van hoe het Gildekwartier gaat worden (2024, ©BPD/Faam Architecten):

{{ img(path="gildekwartier-impressie.jpg", alt="Een luchtfoto met impressie van hoe het Gildekwartier gaat worden (2024) ©BPD/Faam Architecten", class="passend") }}


## Gildehuys

Aan de noordzijde van de Gildebuurt naast [onze buurttuin](activiteiten/buurttuin/) is inmiddels 't Gildehuys verrezen, een nieuw appartementencomplex.
Het Eindhovens dagblad schreef er al in 2020 [een
artikel](https://www.ed.nl/eindhoven/appartementencomplex-t-gildehuys-op-braakliggend-terrein-pastoriestraat-eindhoven~ab31171a/)
over, en ook [de
stentor](https://www.destentor.nl/binnenland/opeens-staat-er-een-flat-in-je-tuin-buurman-in-eindhoven-compleet-verrast~a72645b7/)
schreef over perikelen rond de bouw.

{{ img(path="gildehuys.jpg", alt="'t Gildehuys, een sfeerimpressie van de projectontwikkelaar", class="passend") }}


## Appartementen Woenselse Markt

Op de Woenselse markt naast de Albert Heijn wordt door BurgtBouw een appartementenblok van 21 appartementen inclusief
commerciële ruimte gebouwd.

{{ img(path="burgtbouw.jpg", alt="21 Appartementen", class="passend") }}
