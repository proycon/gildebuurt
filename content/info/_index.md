+++
title = "Over de buurt"
sort_by = "title"
description = "Informatiepagina over de Gildebuurt, Oud Woensel, Eindhoven"
template = "section.html"
+++

## Inleiding

In 2000 splitste de gemeente Oud Woensel op in de Gildebuurt en Hemelrijken.
Sindsdien is de Gildebuurt zogezegd de taartpunt die begrenst ligt tussen de
Kruisstraat, Pastoor Petersstraat, Veldm. Montgomerylaan, Pastorielaan en
Woenselse Markt. Onze directe buur is de wijk Hemelrijken.

Wij wonen in een van de oudste buurten van Eindhoven. In 1922 startte
wooncoöperatie ‘Helpt u zelf door samenwerking’ (de voorloper van Wooninc) hun
eerste bouwproject. In die tijd waren zij het enige protestantse collectief in
het overwegend katholieke Eindhoven.

De Gildebuurt staat niet voor niets op de historische waardekaart van Eindhoven
en is een kleurrijke buurt waar je nog iets van de Gildesfeer terugvindt. Deze
Protestantse ‘enclave’ van toen ademt nog een beetje een sfeer uit van een
eigenheid en heeft meer een dorpskarakter dan een stadsallure.

## Onderwerpen



