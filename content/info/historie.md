+++
title = "Historie"
description = "Een korte schets van de historie van de Gildebuurt"
template = "page.html"

[extra]
thumbnail = "/img/thumb_historie.jpg"

[[extra.gallery]]
caption = "katholieke processie (1924)"
path="img/1924 katholieke processie.jpg"

[[extra.gallery]]
caption = "bakkerstraat 44"
path="img/bakkerstraat 44.jpg"

[[extra.gallery]]
caption = "bij de boom in de bakkerstraat"
path = "img/bij boom in bakkerstraat.jpg"

[[extra.gallery]]
caption = "boot bouwen in de achtertuin (slagerstraat)"
path = "img/boot bouwen in de slagerstraat.jpg"

[[extra.gallery]]
caption = "Bouwjaaroverzicht bakkerstraat en omstreken"
path = "img/bouwjaaroverzichtplattegrond.jpg"

[[extra.gallery]]
caption = "Kruidenierswinkel aan de bakkerstraat"
path = "img/kruidenierswinkel_bakkerstraat.jpg"

[[extra.gallery]]
caption = "Looierstraat 1963"
path = "img/looierstraat1963.jpg"

[[extra.gallery]]
caption = "Luchtfoto Oud-Woensel (rond 1963)"
path = "img/luchtfoto_oud_woensel_rond1963.jpg"

[[extra.gallery]]
caption = "Plattegrond Woensel 1865"
path = "img/plattegrond 1865 HvM.jpg"

[[extra.gallery]]
caption = "Plattegrond Woensel 1924"
path = "img/plattegrond 1924 HvM.jpg"

[[extra.gallery]]
caption = "Renovatieplaquette"
path = "img/renovatieplaquette.jpg"

[[extra.gallery]]
caption = "Sigarenwinkel bakkerstraat"
path = "img/sigarenwinkel.jpg"

[[extra.gallery]]
caption = "Hangjongeren in de spinnerstraat (1965)"
path = "img/spinnerstraat hangjongeren 1965.jpg"

[[extra.gallery]]
caption = "Vuilnisbakrecht"
path = "img/vuilnisbakrecht voorkant.jpg"

[[extra.gallery]]
caption = "Vuilnisbakrecht"
path = "img/vuilnisbakrecht.jpg"

+++

Tot 1920 was Woensel een zelfstandige gemeente. De kern lag al sinds de Middeleeuwen bij de Oude Toren, de vroegere dorpskerk. (De toren staat er nog en daarom heen ligt een kerkhof). De nederzetting schoof later meer op naar noordwestelijke richting naar de rand van de zandrug, die het beloop volgt van de Frankrijkstraat, Woenselsestraat en Vlokhovenseweg.

Ten zuiden van deze zandrug ligt, wat men nu aanduidt als Oud Woensel, onze buurt Gildebuurt. Vlak bij het beekdal waar omstreeks 1232 de stad Eindhoven werd gesticht op de plek waar de Dommel en Gender samenkomen.

Vele straatnamen werden na de annexatie van Woensel in 1920 veranderd omdat in Eindhoven soortgelijke namen voorkwamen, maar de Kruisstraat bleef historisch gezien de belangrijkste weg.

De oude wegenstructuur verdween vooral na de Tweede Wereldoorlog aan de zuidkant als gevolg van de aanleg van het Hoogspoor en de aanleg van het huidige Fellenoord. De Broekseweg werd afgebroken en de Veldmaarschalk Montgomerylaan verscheen.

## Bebouwing

Na 1850 werd er als gevolg van industrialisatie en verstedelijking steeds meer gebouwd. In 1874 kwam er een nieuwe St. Petruskerk aan de Kloosterdreef met het gevolg dat er een nieuwe dorpcentrum ontstond met tegenover de kerk een nieuw gemeentehuis (1896) Langs de hoofdwegen ontstond er kleinschalige bedrijvigheid.

In 1920 kampte Woensel net als de dorpen Gestel, Stratum, Strijp, Tongelre en Eindhoven met enorme problemen op planologisch, sociaal en financieel gebied. Zij beschikten niet over de financiële middelen en ruimtes om de arbeiders goed te vestigen die uit andere delen van het land kwamen om hier te werken. Om deze problemen aan te pakken vormden ze in 1920 één gemeente Eindhoven. Het toenmalig dorp Eindhoven, zelf krap 6500 inwoners, nam toe met bijna 40.000 inwoners en met 6.230 hectaren.

De sigaren- en textielindustrie groeiden in de beginjaren van 1900 flink en ook Philips gloeilampen zocht goedkope werkkrachten. Vele arbeiders kwamen vanuit Drenthe, Twente en Achterhoek deze kant uit omdat daar hongersnood heerste. Velen waren protestant of Joods. Dit ging niet goed samen in het grotendeels Katholieke Eindhoven.

{{ img(path="helpt-uzelf.jpg", class="rechts klein") }}

Zo ontstond er rond 1920 een protestants collectief *“Helpt uzelf door samenwerking”* die, als er weer geld was in de jaren 1922-1924-1927, rechts van de Kruisstraat bouwden voor de gewone man; wevers, verwers, looiers, bakkers, schilders, kruideniers, timmermannen, klokkengieters en handwerkers. Dit collectief was de voorloper van de huidige corporatie Wooninc die nog steeds 119 huizen verhuurt aan grotendeels de lager opgeleiden, arbeiders of studerenden in de straten Bakkerstraat, Wassenaarstraat, Slagerstraat, Verwerstraat, en voor een gedeelte de Pastoriestraat ( deze straat is grotendeels gesloopt in 2018 in verband met de aanleg van de HOV lijn).

Of de naam ‘Gildebuurt’ toen ontstaan is, is slechts een vermoeden, maar speciaal was dat de Gildebuurt hét conclave in heel Eindhoven was waar protestanten hun thuis hadden. Dit is wel een aantekening in de historie van Eindhoven waard. Een uniek stukje geschiedenis die bewaard moet blijven! ( Vandaar dat de Gildebuurt op de historische waardekaart staat sinds enkele jaren).

Uit verhalen van oud-bewoners heb ik begrepen dat er velen uit Twente en de Achterhoek kwamen. Streng protestants, meestal grote gezinnen, die hard wilden werken en die achter het huis hun eigen groentes teelden en zelfs een varken of konijnen hielden om in hun eigen levensonderhoud te kunnen voorzien, want ze verdienden niet veel . De Kruisstraat was letterlijk de scheidingslijn tussen de Katholieke -en Protestantse bevolking. Het was volgens de lokale notabelen ongepast om als katholiek omgang te hebben met een protestant. De huidige school *“de Driestam”* was in die jaren de school waar de protestantse en andersgelovige kinderen naar toe gingen. De katholieke leerlingen gingen naar de school aan de Pastoriestraat. Jongens en meisjes gescheiden natuurlijk.

De kleine zelfstandigen kochten huizen in het andere gedeelte van de Pastoriestraat en de Schoenmakerstraat waar een beroemde klokkenmaker woonde.

Met het bouwen van de Montgomerylaan in het jaar 1957 verschenen er flats.

Op de plattegrond van 1937 zie je duidelijk de lege plaatsen waar in de jaren 50> 60 de Spinnerstraat, Looierstraat en Weverstraat ontstond ;gebouwd door Woonstichting Trudo die momenteel steeds meer huizen verkoopt.

De kinderen uit de Gildebuurt vonden in de jaren ’30-40-’50 hun speeltuin enorm, want ze konden ravotten vanaf de Verwerstraat tot aan de Dommel. *“Alles was nog braakliggend of stond vol met graan. Een heerlijke tijd als ik hieraan terugdenk” aldus toen jongetje Viveen”* die niet wil denken aan de bommen die sommige huizen van de kaart vervaagden en waar plotseling sommige gezinnen uit de vrije hechte gemeenschap verdwenen waren.

Een stille getuigen zijn de lege plek op het hoekje Wassenaarstraat/Pastoriestraat en de stoepstenen in de Pastoriestaat.
